const Sequelize = require('sequelize');
const BaseModel = require('./base');

module.exports = class UserModel extends BaseModel {
	static get modelName() {
		return "user";
	}

	static get properties() {
		let props = {
			username: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true
			},
			password: {
				type: Sequelize.STRING,
				allowNull: false
			},
			salt: {
				type: Sequelize.STRING,
				allowNull: false
			}
		};

		return Object.assign(super.properties, props);
	}
};