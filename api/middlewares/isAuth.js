const AuthService = require("../../services/AuthService");

module.exports = async (req, res, next) => {

	let token;

	/**
	 * @TODO Edge and Internet Explorer do some weird things with the headers
	 * So I believe that this should handle more 'edge' cases ;)
	 */
	if (
		(req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Token') ||
		(req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
	) {
		token = req.headers.authorization.split(' ')[1];
	}

	let user = await AuthService.isAuth({ token });
	if (user) {
		// contains response local variables scoped to the request
		// http://expressjs.com/en/api.html#res.locals
		res.locals.user = user;
		next();
	}
	else {
		res.status(403).json({ message: "Unauthenticated" });
	}
}