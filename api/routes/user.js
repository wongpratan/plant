const IsAuth = require("../middlewares/isAuth");
const UserService = require("../../services/UserService");

module.exports = class userRoute {
	constructor(app) {
		this.app = app;
	}

	init() {

		this.app.post('/user', async (req, res) => {
			try {
				await UserService.create(req.body);
				res.send();
			}
			catch (e) {
				// TODO: Logging
				console.error(e);
				res.status(500);
			}
		});

		this.app.put('/user/password', IsAuth, async (req, res) => {
			try {
				await UserService.changePassword({
					user: res.locals.user,
					password: req.body.password,
					newPassword: req.body.newPassword
				});
				res.send();
			}
			catch (e) {
				// TODO: Logging
				console.error(e);
				res.status(500);
			}

		});

		this.app.put('/user/resetpassword', IsAuth, async (req, res) => {
			try {
				await UserService.resetPassword(req.body);
				res.send();
			}
			catch (e) {
				// TODO: Logging
				console.error(e);
				res.status(500);
			}
		});
	}
};