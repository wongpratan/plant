const AuthService = require("../../services/AuthService");

module.exports = class authRoute {
	constructor(app) {
		this.app = app;
	}

	init() {

		this.app.post('/login', async (req, res) => {

			let token = await AuthService.isValid(req.body);
			if (token) {
				res.json({
					token: token
				});
			}
			else {
				res.status(401).json({ message: "Username or Password is invalid" });
			}
		});

	}
}