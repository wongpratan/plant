import { JetView } from "webix-jet";

export default class HeaderView extends JetView {
	config() {
		let ui = {
			view: "toolbar",
			padding: 3,
			elements: [
				{
					view: "button",
					type: "icon",
					icon: "mdi mdi-menu",
					width: 37,
					align: "left",
					css: "app_button",
					click: () => {
						this.app.callEvent("header:hamburger:click");
					}
				},
				{ view: "label", label: "ระบบฐานข้อมูลพันธุ์พืชป่าชายหาด บริเวณ มทร.ศรีวิชัย วิทยาเขตตรัง" },
				// { fillspace: true },
				// { view: "button", type: "icon", width: 45, css: "app_button", icon: "mdi mdi-comment", badge: 4 },
				// { view: "button", type: "icon", width: 45, css: "app_button", icon: "mdi mdi-bell", badge: 10 }
			]
		};

		return ui;
	}
}