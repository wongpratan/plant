import { JetView, plugins } from "webix-jet";

export default class MenuView extends JetView {
	config() {
		let menu = {
			id: "side:menu",
			name: "sidemenu",
			view: "sidebar",
			collapsedWidth: 40,
			select: true,
			collapsed: true,
			template: "<span class='webix_icon #icon# mdi-22px'></span> &nbsp;#value# ",
			data: [
				{ value: "Dashboard", id: "start", icon: "mdi mdi-grid" },
				{ value: "Data", id: "data", icon: "mdi mdi-pencil" },
				{ value: "Form", id: "form", icon: "mdi mdi-checkbook" }
			]
		};

		return menu;
	}

	init() {
		this.use(plugins.Menu, "side:menu");
	}

}