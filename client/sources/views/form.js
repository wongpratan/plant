import { JetView, plugins } from "webix-jet";

export default class FormView extends JetView {
	config() {
		let form = {
			id: "form",
			view: "form",
			elements: [
				{
					view: "text",
					label: "Name",
					required: true
				},
				{
					view: "button",
					value: "Save"
				}
			]
		};

		return form;
	}

	init(view) {
		// this.use(plugins.UnloadGuard, () => {

		// 	if (view.validate())
		// 		return true;

		// 	return webix.confirm({
		// 		title: "Form is incomplete",
		// 		text: "Do you still want to leave?"
		// 	});

		// });
	}

}