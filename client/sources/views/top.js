import { JetView, plugins } from "webix-jet";

import HeaderView from "./header";
import MenuView from "./menu";

export default class TopView extends JetView {
	config() {

		let ui = {
			type: "clean",
			css: "app_layout",
			rows: [
				HeaderView,
				{
					cols: [
						MenuView,
						{
							type: "wide",
							paddingY: 10,
							paddingX: 10,
							rows: [
								{ $subview: true }
							]
						}
					]
				}
			]
		};

		return ui;
	}

	init() {
		this.app.attachEvent("header:hamburger:click", () => {
			this.webix.$$("side:menu").toggle();
		});
	}

}