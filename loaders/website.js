const bodyParser = require("body-parser");
const express = require("express");
const cors = require("cors");
const path = require('path');

const authRoute = require("../api/routes/auth");
const userRoute = require("../api/routes/user");

module.exports = class WebLoader {
	constructor(app) {
		this.app = app;

		this.authRoute = new authRoute(app);
		this.userRoute = new userRoute(app);
	}

	init() {
		// view engine
		this.app.set('view engine', 'ejs');

		// define html/js/css folder
		this.app.use(express.static(__dirname + '/../client/codebase'));
		this.app.use(express.static(__dirname + '/../client/bower_components'));

		// homepage
		this.app.get('/', (req, res) => {
			res.render(path.resolve('client/index.ejs'), {
				PRODUCTION: process.env.PRODUCTION
			});
		});

		// parse application/x-www-form-urlencoded
		this.app.use(bodyParser.urlencoded({ extended: false }));

		// parse application/json
		this.app.use(bodyParser.json());

		// CORS
		this.app.use(cors());

		this.authRoute.init();
		this.userRoute.init();

		this.app.listen(process.env.PORT);
	}
}