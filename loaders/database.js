const Sequelize = require('sequelize');

const UserModel = require('../models/user');

module.exports = class DatabaseLoader {
	constructor() {
		this.sequelize = new Sequelize(
			process.env.MYSQL_DATABASE,
			process.env.MYSQL_USER,
			process.env.MYSQL_PASS,
			{
				host: process.env.MYSQL_HOST,
				port: process.env.MYSQL_PORT,
				dialect: 'mysql'
			}
		);
	}

	init() {
		UserModel.init(this.sequelize);
	}

	createTables() {
		UserModel.createTable(this.sequelize);
	}
}
