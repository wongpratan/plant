const dotenv = require('dotenv');
const express = require("express");
const WebLoader = require("./loaders/website");
const DBLoader = require("./loaders/database");

// Config
if (process.env.NODE_ENV !== 'production') dotenv.config();

// Website
const app = express();
let webLoader = new WebLoader(app);
webLoader.init();

// Database
let dbLoader = new DBLoader();
dbLoader.init();