const crypto = require("crypto");
const UserModel = require("../models/user");
const AuthService = require("./AuthService");

function getGenerateSalt() {
	return crypto.randomBytes(16).toString('base64');
}

function getRandomPassword() {
	return Math.random().toString(36).slice(-8);
}

module.exports = class UserService {

	static async create({ username, password }) {

		if (!username || !password)
			throw new TypeError("Invalid parameters");

		// check duplicate username
		let user = await UserModel.findOne({
			where: { username: username }
		});
		if (user)
			throw new TypeError("Username is duplicate");

		// generate hashed password
		let salt = getGenerateSalt();
		let hashedPassword = AuthService.getHashedPassword(password, salt);

		await UserModel.create({
			username: username,
			password: hashedPassword,
			salt: salt
		});
	}


	static async changePassword({ user, password, newPassword }) {

		if (!password || !newPassword)
			throw new TypeError("Invalid parameters");

		if (!user)
			throw new TypeError("Username or Password is invalid");

		let userData = user.toJSON();

		// Invalid
		let hashedPassword = AuthService.getHashedPassword({ password, salt: userData.salt });
		if (userData.password != hashedPassword)
			throw new TypeError("Username or Password is invalid");

		// New password
		let newHashedPassword = AuthService.getHashedPassword({
			password: newPassword, 
			salt: userData.salt
		});
		user.password = newHashedPassword;

		await user.save();
	}

	/**
	 * @method resetPassword
	 * 
	 * @return {string} newPassword
	 */
	static async resetPassword({ username }) {

		if (!username)
			throw new TypeError("Invalid parameters");

		// Pull user from username
		let user = await UserModel.findOne({
			where: { username: username }
		});
		if (!user)
			throw new TypeError("Invalid username");

		let userData = user.toJSON();

		// Update new password
		let newPassword = getRandomPassword();
		let hashedPassword = AuthService.getHashedPassword(newPassword, userData.salt);
		user.password = hashedPassword;

		await user.save();

		return newPassword;
	}
}