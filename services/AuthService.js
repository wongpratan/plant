const crypto = require('crypto');
const jwt = require("jwt-simple");
const User = require("../models/user");

module.exports = class AuthService {

	/**
	 * @method isAuth
	 * 
	 * @param {Object} {
	 * 						token: {string}
	 * 					}
	 * @return {User} - UserModel
	 */
	static async isAuth({ token }) {
		if (!token) return false;

		let payload = jwt.decode(token, process.env.JWT_SECRET);

		let user = await User.findOne({
			where: { id: payload.sub }
		});

		return user;
	}

	/**
	 * @method isValid
	 * 
	 * @param {Object} {
	 * 						username: {string},
	 * 						password: {string}
	 * 					}
	 * @return {?string} token
	 */
	static async isValid({ username, password }) {
		if (!username || !password) return;

		let user = await User.findOne({
			where: { username: username }
		});
		if (!user) return;

		let userData = user.toJSON();

		// Encode MD5
		let hashedPassword = this.getHashedPassword({
			password: password, 
			salt: userData.salt
		});

		// Invalid
		if (userData.password != hashedPassword) return;

		// Valid
		let payload = {
			sub: userData.id,
			iat: new Date().getTime() // issue at time
		};

		return jwt.encode(payload, process.env.JWT_SECRET);
	}

	/**
	 * @method getHashedPassword
	 * 
	 * @param {Object} {
	 * 						password: {string},
	 * 						salt: {string}
	 * 					}
	 * @return {string} hashedPassword
	 */
	static getHashedPassword({ password, salt }) {
		if (!password || !salt)
			throw new TypeError("Invalid parameters");

		let passwordSalt = `${password}${salt}`;

		// Encode MD5
		return crypto.createHash('md5').update(passwordSalt).digest("hex");
	}

};